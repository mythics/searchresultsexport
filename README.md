﻿# SearchResultsExport

## Component Information
* Created by: Mythics
* Author: Jonathan Hult
* License: MIT
* Last Updated: build_1_20140327

## Overview
This WebCenter Content component adds Excel and CSV export functionality for FrameworkFolders and Searches. It allows export of the current page or all results.
	
* Dynamichtml includes:
	- custom_searchapi_result_menus_setup: Core - add export menu items to Search page Query Actions menu

* Services:
	- GET_SEARCH_RESULTS_EXPORT
	Parameters:
	* numResults: "currentPage" (for current page) or "all" (for all results); only retrieves results for specified query and what the current user has security access to
	* fileFormat: "excel" or "csv"
	* isUseFieldCaption: boolean determining whether field caption is used for column header; default is true
	* QueryText: QueryText to be used
	* StartRow: StartRow to be used (only if numResults is "currentPage"); default is 1
	* ResultCount: ResultCount to be used (only if numReults is "currentPage"); default is 20
	* fileName: the name of the file to be downloaded; default is Search Results.extension
	
* ServiceHandlers:
	- SearchService: com.mythics.webcenter.content.searchresultsexport.SearchResultExportServiceHandler

* Strings
	- wwSearchResultsExportCurrentPageExcel
	- wwSearchResultsExportAllResultsExcel
	- wwSearchResultsExportCurrentPageCSV
	- wwSearchResultsExportAllResultsCSV
	- wwSearchResultsExportMustSpecifyNumResultsParam
	- wwSearchResultsExportMustSpecifyNumResultsValue
	- wwSearchResultsExportMustSpecifyFileFormatParam
	- wwSearchResultsExportMustSpecifyFileFormatValue
	
* Tracing sections:
	- searchresultsexport

* Preference Prompts:
	- SearchResultsExport_ComponentEnabled: boolean determining whether component functionality is enabled.
	
## Compatibility
This component was built upon and tested on the version listed below, this is the only version it is known to work on, but it may work on older/newer versions.

* 11.1.1.8.1DEV-2014-01-06 04:18:30Z-r114490 (Build:7.3.5.185)

## Changelog
* build_1_20140327
	- Initial component release