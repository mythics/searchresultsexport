package com.mythics.webcenter.content.searchresultsexport;

import static com.mythics.webcenter.content.WebCenterContentStringConstants.FIELD_CAPTION;
import static com.mythics.webcenter.content.searchresultsexport.SearchResultExportStringConstants.MSG_NO_RESULTS_FOUND;
import static com.mythics.webcenter.content.searchresultsexport.SearchResultExportStringConstants.SEARCH_RESULTS_STR;
import static com.mythics.webcenter.content.searchresultsexport.SearchResultExportStringConstants.TRACE_SECTION;
import static intradoc.common.FileUtils.openDataWriter;
import static intradoc.common.LocaleUtils.encodeMessage;
import static intradoc.data.DataBinder.getNextFileCounter;
import static intradoc.data.DataBinder.getTemporaryDirectory;
import intradoc.common.ServiceException;
import intradoc.data.DataResultSet;
import intradoc.server.Service;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import com.mythics.webcenter.content.WebCenterContentHelperUtils;

/**
 * @author Jonathan Hult
 */
public class SearchResultExportUtils {

	/**
	 * Write CSV file from DataResultSet.
	 * 
	 * @param drs
	 *            DataResultSet containing Search Results information.
	 * @param isUseFieldCaption
	 *            True if the field caption should be used as the header column.
	 * @param localData
	 *            Properties which contains field caption.
	 * @param service
	 *            Service to be used to retrieve field caption.
	 * @return String containing path to CSV file.
	 * @throws IOException
	 */
	static String writeCSVFile(final DataResultSet drs, final boolean isUseFieldCaption, final Properties localData, final Service service) throws ServiceException {
		traceVerbose("Enter writeCSVFile");

		try {
			String filePath = null;

			if (drs != null && !drs.isEmpty()) {
				filePath = getTemporaryDirectory() + getNextFileCounter();
				trace("filePath: " + filePath);

				if (drs.first()) {
					final CSVPrinter csvp = new CSVPrinter(openDataWriter(filePath), CSVFormat.DEFAULT);

					// Print header row
					final List<String> header = new ArrayList<String>();
					for (int colNum = 0; colNum < drs.getNumFields(); colNum++) {
						// Get field name
						String fieldName = drs.getFieldName(colNum);
						traceVerbose("fieldName before caption: " + fieldName);

						if (isUseFieldCaption) {
							fieldName = localData.getProperty(fieldName + ":" + FIELD_CAPTION);
							traceVerbose("fieldName as caption: " + fieldName);

							if (fieldName == null || fieldName.trim().isEmpty()) {
								fieldName = drs.getFieldName(colNum);
							}
						}
						header.add(fieldName);
						traceVerbose("Field " + colNum + ": " + fieldName);
						traceVerbose("-------------------------------------------------------------");
					}
					csvp.printRecord(header);
					trace("Header: " + header.toString());

					// Loop content items and print each as a record/row
					for (drs.first(); drs.isRowPresent(); drs.next()) {
						csvp.printRecord(drs.getCurrentRowAsList());
					}
					csvp.close();
					trace("Successfully wrote file to: " + filePath);
				} else {
					throw new ServiceException(encodeMessage(MSG_NO_RESULTS_FOUND, null));
				}
			}
			return filePath;
		} catch (final IOException e) {
			throw new ServiceException(e);
		} finally {
			traceVerbose("Exit writeCSVFile");
		}
	}

	/**
	 * Write Excel file from DataResultSet.
	 * 
	 * @param drs
	 *            DataResultSet containing Search Results information.
	 * @param isUseFieldCaption
	 *            True if the field caption should be used as the header column.
	 * @param localData
	 *            Properties which contains field caption.
	 * @param service
	 *            Service to be used to retrieve field caption.
	 * @return String containing path to Excel file.
	 * @throws IOException
	 * @throws ServiceException
	 */
	static String writeExcelFile(final DataResultSet drs, final boolean isUseFieldCaption, final Properties localData, final Service service) throws ServiceException {
		traceVerbose("Enter writeExcelFile");

		try {
			String filePath = null;

			if (drs != null && !drs.isEmpty()) {
				if (drs.first()) {
					// keep 100 rows in memory, exceeding rows will be flushed
					// to
					// disk
					final SXSSFWorkbook wb = new SXSSFWorkbook(100);
					final Sheet sh = wb.createSheet(SEARCH_RESULTS_STR);

					// Create header row (will always be 0 for header)
					Row row = sh.createRow(0);

					for (int colNum = 0; colNum < drs.getNumFields(); colNum++) {
						// Get field name
						String fieldName = drs.getFieldName(colNum);
						traceVerbose("fieldName before caption: " + fieldName);

						if (isUseFieldCaption) {
							fieldName = localData.getProperty(fieldName + ":" + FIELD_CAPTION);
							traceVerbose("fieldName as caption: " + fieldName);

							if (fieldName == null || fieldName.trim().isEmpty()) {
								fieldName = drs.getFieldName(colNum);
							}
						}
						final Cell cell = row.createCell(colNum);
						cell.setCellValue(fieldName);
						traceVerbose("Field " + colNum + ": " + fieldName);
						traceVerbose("-------------------------------------------------------------");
					}

					for (drs.first(); drs.isRowPresent(); drs.next()) {
						// Create row
						row = sh.createRow(drs.getCurrentRow() + 1);

						// Content item records
						for (int colNum = 0; colNum < drs.getNumFields(); colNum++) {
							final Cell cell = row.createCell(colNum);
							cell.setCellValue(drs.getStringValue(colNum));
						}
					}

					// Used for header filter
					final int endRow = sh.getLastRowNum();
					final int endCol = sh.getRow(endRow).getLastCellNum() - 1;

					// Set header filter
					sh.setAutoFilter(new CellRangeAddress(0, endRow, 0, endCol));

					filePath = getTemporaryDirectory() + getNextFileCounter();
					trace("filePath: " + filePath);
					final FileOutputStream out = new FileOutputStream(filePath);
					wb.write(out);
					out.close();

					// dispose of temporary files backing this workbook on disk
					wb.dispose();

					trace("Successfully wrote file to: " + filePath);

				} else {
					throw new ServiceException(encodeMessage(MSG_NO_RESULTS_FOUND, null));
				}
			}

			return filePath;
		} catch (final IOException e) {
			throw new ServiceException(e);
		} finally {
			traceVerbose("Exit writeExcelFile");
		}
	}

	static void trace(final String message) {
		trace(message, null);
	}

	static void trace(final String message, final Exception e) {
		WebCenterContentHelperUtils.trace(message, TRACE_SECTION, e);
	}

	static void traceVerbose(final String message) {
		WebCenterContentHelperUtils.traceVerbose(message, TRACE_SECTION);
	}

	static void warn(final String message, final Exception e) {
		WebCenterContentHelperUtils.trace(message, TRACE_SECTION, e);
		WebCenterContentHelperUtils.warn(message, TRACE_SECTION, e);
	}
}
