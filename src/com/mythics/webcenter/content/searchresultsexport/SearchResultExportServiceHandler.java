package com.mythics.webcenter.content.searchresultsexport;

import static com.mythics.webcenter.content.WebCenterContentHelperUtils.getAllSearchResults;
import static com.mythics.webcenter.content.WebCenterContentHelperUtils.getSearchResults;
import static com.mythics.webcenter.content.WebCenterContentStringConstants.DESC;
import static com.mythics.webcenter.content.WebCenterContentStringConstants.D_IN_DATE;
import static com.mythics.webcenter.content.WebCenterContentStringConstants.PAGE_MERGER;
import static com.mythics.webcenter.content.WebCenterContentStringConstants.QUERY_TEXT;
import static com.mythics.webcenter.content.WebCenterContentStringConstants.RESULT_COUNT;
import static com.mythics.webcenter.content.WebCenterContentStringConstants.SEARCH_RESULTS;
import static com.mythics.webcenter.content.WebCenterContentStringConstants.SORT_FIELD;
import static com.mythics.webcenter.content.WebCenterContentStringConstants.SORT_ORDER;
import static com.mythics.webcenter.content.WebCenterContentStringConstants.START_ROW;
import static com.mythics.webcenter.content.searchresultsexport.SearchResultExportStringConstants.ALL;
import static com.mythics.webcenter.content.searchresultsexport.SearchResultExportStringConstants.COMPONENT_ENABLED;
import static com.mythics.webcenter.content.searchresultsexport.SearchResultExportStringConstants.CSV;
import static com.mythics.webcenter.content.searchresultsexport.SearchResultExportStringConstants.CURRENT_PAGE;
import static com.mythics.webcenter.content.searchresultsexport.SearchResultExportStringConstants.DOT_CSV;
import static com.mythics.webcenter.content.searchresultsexport.SearchResultExportStringConstants.DOT_XLS;
import static com.mythics.webcenter.content.searchresultsexport.SearchResultExportStringConstants.EXCEL;
import static com.mythics.webcenter.content.searchresultsexport.SearchResultExportStringConstants.FILE_FORMAT;
import static com.mythics.webcenter.content.searchresultsexport.SearchResultExportStringConstants.FILE_NAME;
import static com.mythics.webcenter.content.searchresultsexport.SearchResultExportStringConstants.IS_USE_PAGE_CAPTION;
import static com.mythics.webcenter.content.searchresultsexport.SearchResultExportStringConstants.MSG_MUST_SPECIFY_FILE_FORMAT_PARAM;
import static com.mythics.webcenter.content.searchresultsexport.SearchResultExportStringConstants.MSG_MUST_SPECIFY_FILE_FORMAT_VALUE;
import static com.mythics.webcenter.content.searchresultsexport.SearchResultExportStringConstants.MSG_MUST_SPECIFY_NUM_RESULTS_PARAM;
import static com.mythics.webcenter.content.searchresultsexport.SearchResultExportStringConstants.MSG_MUST_SPECIFY_NUM_RESULTS_VALUE;
import static com.mythics.webcenter.content.searchresultsexport.SearchResultExportStringConstants.MSG_NO_RESULTS_FOUND;
import static com.mythics.webcenter.content.searchresultsexport.SearchResultExportStringConstants.NUM_RESULTS;
import static com.mythics.webcenter.content.searchresultsexport.SearchResultExportStringConstants.SEARCH_RESULTS_STR;
import static com.mythics.webcenter.content.searchresultsexport.SearchResultExportUtils.trace;
import static com.mythics.webcenter.content.searchresultsexport.SearchResultExportUtils.traceVerbose;
import static com.mythics.webcenter.content.searchresultsexport.SearchResultExportUtils.writeCSVFile;
import static com.mythics.webcenter.content.searchresultsexport.SearchResultExportUtils.writeExcelFile;
import static intradoc.common.LocaleUtils.encodeMessage;
import static intradoc.data.DataBinderUtils.getLocalBoolean;
import static intradoc.data.DataBinderUtils.getLocalInteger;
import static intradoc.shared.SharedObjects.getEnvValueAsBoolean;
import intradoc.common.DataStreamWrapper;
import intradoc.common.ServiceException;
import intradoc.data.DataBinder;
import intradoc.data.DataException;
import intradoc.data.DataResultSet;
import intradoc.server.DocProfileManager;
import intradoc.server.PageMerger;
import intradoc.server.ServiceHandler;

import java.util.Properties;

/**
 * @author Jonathan Hult
 */
public class SearchResultExportServiceHandler extends ServiceHandler {

	private String numResults;
	private String fileFormat;
	private boolean isUseFieldCaption;
	private String queryText;
	private int startRow;
	private int resultCount;
	private String sortField;
	private String sortOrder;
	private DataResultSet searchResults;
	private String filePath;
	private String fileName;
	private long startTimeInNanos;
	private Properties localData;

	public void checkComponentEnabled() throws ServiceException {
		traceVerbose("Enter checkComponentEnabled");

		this.startTimeInNanos = System.nanoTime();
		try {
			// Only allow service to run if component preference prompt is
			// enabled
			if (!getEnvValueAsBoolean(COMPONENT_ENABLED, false)) {
				throw new ServiceException("Excel Export functionality is not enabled.");
			}
		} finally {
			traceVerbose("Exit checkComponentEnabled");
		}
	}

	/**
	 * Verify service parameters and set as private member variables.
	 * 
	 * @throws ServiceException
	 */
	public void setAndVerifyParams() throws ServiceException {
		traceVerbose("Enter setAndVerifyParams");

		try {
			final String numResults = m_binder.getLocal(NUM_RESULTS);
			if (numResults == null) {
				throw new ServiceException(encodeMessage(MSG_MUST_SPECIFY_NUM_RESULTS_PARAM, null));
			} else if (!numResults.equals(ALL) && !numResults.equals(CURRENT_PAGE)) {
				throw new ServiceException(encodeMessage(MSG_MUST_SPECIFY_NUM_RESULTS_VALUE, null));
			}
			this.numResults = numResults;
			trace("numResults: " + this.numResults);

			String fileExtension = "";

			final String fileFormat = m_binder.getLocal(FILE_FORMAT);
			if (fileFormat == null) {
				throw new ServiceException(encodeMessage(MSG_MUST_SPECIFY_FILE_FORMAT_PARAM, null));
			} else if (!fileFormat.equals(EXCEL) && !fileFormat.equals(CSV)) {
				throw new ServiceException(encodeMessage(MSG_MUST_SPECIFY_FILE_FORMAT_VALUE, null));
			} else if (fileFormat.equals(EXCEL)) {
				fileExtension = DOT_XLS;
			} else if (fileFormat.equals(CSV)) {
				fileExtension = DOT_CSV;
			}
			this.fileFormat = fileFormat;
			trace("fileFormat: " + this.fileFormat);
			trace("fileExtension: " + fileExtension);

			this.isUseFieldCaption = getLocalBoolean(m_binder, IS_USE_PAGE_CAPTION, true);
			trace("isUseFieldCaption: " + this.isUseFieldCaption);

			final String queryText = m_binder.getLocal(QUERY_TEXT);
			if (queryText == null || queryText.trim().isEmpty()) {
				this.queryText = "";
			} else {
				this.queryText = queryText;
			}
			trace("queryText: " + this.queryText);

			// Get/parse StartRow
			// Default to 1 if there are issues
			this.startRow = getLocalInteger(m_binder, START_ROW, 1);
			trace("startRow: " + this.startRow);

			// Get/parse ResultCount
			// Default to 20 if there are issues
			this.resultCount = getLocalInteger(m_binder, RESULT_COUNT, 20);
			trace("resultCount: " + this.resultCount);

			// Set default SortField to dInDate
			this.sortField = m_binder.getLocal(SORT_FIELD);
			if (this.sortField == null || this.sortField.trim().isEmpty()) {
				this.sortField = D_IN_DATE;
			}
			trace("sortField: " + this.sortField);

			// Set default SortOrder to Desc
			this.sortOrder = m_binder.getLocal(SORT_ORDER);
			if (this.sortOrder == null || this.sortOrder.trim().isEmpty()) {
				this.sortOrder = DESC;
			}
			trace("sortOrder: " + this.sortOrder);

			// Set default filename to Search Results.extension
			final String fileName = m_binder.getLocal(FILE_NAME);
			if (fileName == null || !fileName.trim().isEmpty()) {
				this.fileName = SEARCH_RESULTS_STR;
			}
			this.fileName += fileExtension;
			trace("fileName: " + this.fileName);

			this.localData = new Properties();
		} finally {
			traceVerbose("Exit setAndVerifyParams");
		}
	}

	/**
	 * Get Search Results as DataResultSet. This will either be all search results for specified QueryText or just the current page depending on the value of parameter results.
	 * 
	 * @throws ServiceException
	 * @throws DataException
	 */
	public void getAndSetSearchInfo() throws ServiceException, DataException {
		traceVerbose("Enter getAndSetSearchInfo");

		try {
			DataBinder serviceBinder;
			if (numResults.equals(ALL)) {
				// Get all results
				// This could take a very long time depending on how many
				// content
				// items exist
				serviceBinder = getAllSearchResults(queryText, this.sortField, this.sortOrder, m_service);
			} else {
				// Get current page results
				serviceBinder = getSearchResults(queryText, this.startRow, this.resultCount, this.sortField, this.sortOrder, m_service);
			}
			this.searchResults = new DataResultSet();
			this.searchResults.copy(serviceBinder.getResultSet(SEARCH_RESULTS));

			if (this.searchResults.getNumRows() < 1) {
				throw new ServiceException(encodeMessage(MSG_NO_RESULTS_FOUND, null));
			}

			// Evaluate global rules and profile
			// Need PageMerger so dpAction is not null
			final PageMerger pageMerger = new PageMerger(serviceBinder, m_service);
			m_service.setCachedObject(PAGE_MERGER, pageMerger);
			DocProfileManager.loadDocumentProfile(serviceBinder, m_service, true);
			this.localData = serviceBinder.getLocalData();
			traceVerbose("LocalData after evaluateGlobalRulesAndProfile: " + this.localData.toString());
		} finally {
			traceVerbose("Exit getAndSetSearchInfo");
		}
	}

	/**
	 * Write the file and return
	 * 
	 * @throws ServiceException
	 */
	public void writeFile() throws ServiceException {
		traceVerbose("Enter writeFile");

		try {
			if (fileFormat.equals(EXCEL)) {
				filePath = writeExcelFile(searchResults, this.isUseFieldCaption, localData, m_service);
			} else if (fileFormat.equals(CSV)) {
				filePath = writeCSVFile(searchResults, this.isUseFieldCaption, localData, m_service);
			}
		} finally {
			traceVerbose("Exit writeFile");
		}
	}

	/**
	 * Stream file to user.
	 * 
	 * @throws ServiceException
	 * @throws DataException
	 */
	public void sendFileResponse() throws ServiceException, DataException {
		traceVerbose("Enter sendFileResponse");

		try {
			final DataStreamWrapper streamWrapper = new DataStreamWrapper(filePath, fileName, null);
			streamWrapper.m_useStream = true;
			m_service.setDownloadStream(streamWrapper);
			m_binder.m_isJava = false;
			m_service.setBinder(m_binder);
		} finally {
			traceVerbose("Exit sendFileResponse");
		}
	}
}
