package com.mythics.webcenter.content.searchresultsexport;

/**
 * @author Jonathan Hult
 */
public class SearchResultExportStringConstants {

	// Component
	static final String TRACE_SECTION = "searchresultsexport";
	static final String COMPONENT_ENABLED = "SearchResultsExport_ComponentEnabled";
	static final String NUM_RESULTS = "numResults";
	static final String FILE_FORMAT = "fileFormat";
	static final String EXCEL = "excel";
	static final String CSV = "csv";
	static final String CURRENT_PAGE = "currentPage";
	static final String ALL = "all";
	static final String IS_USE_PAGE_CAPTION = "isUseFieldCaption";
	static final String FILE_NAME = "fileName";

	// File
	static final String SEARCH_RESULTS_STR = "Search Results";
	static final String DOT_XLS = ".xlsx";
	static final String DOT_CSV = ".csv";

	// Messages
	static final String MSG_MUST_SPECIFY_NUM_RESULTS_PARAM = "wwSearchResultsExportMustSpecifyNumResultsParam";
	static final String MSG_MUST_SPECIFY_NUM_RESULTS_VALUE = "wwSearchResultsExportMustSpecifyNumResultsValue";
	static final String MSG_MUST_SPECIFY_FILE_FORMAT_PARAM = "wwSearchResultsExportMustSpecifyFileFormatParam";
	static final String MSG_MUST_SPECIFY_FILE_FORMAT_VALUE = "wwSearchResultsExportMustSpecifyFileFormatValue";
	static final String MSG_NO_RESULTS_FOUND = "wwSearchResultsExportNoResultsFound";
}
