package com.mythics.webcenter.content;

/**
 * @author Jonathan Hult
 */
public class WebCenterContentStringConstants {
	// OOTB
	public static final String IDC_SERVICE = "IdcService";
	public static final String CHECKIN_UNIVERSAL = "CHECKIN_UNIVERSAL";
	public static final String PRIMARY_FILE_PATH = "primaryFile:path";
	public static final String PRIMARY_FILE = "primaryFile";
	public static final String IS_AUTO_NUMBER = "IsAutoNumber";
	public static final String PRIMARY = "Primary";
	public static final String LATEST = "Latest";
	public static final String D_DOC_NAME = "dDocName";
	public static final String DOC_INFO = "DOC_INFO";
	public static final String DOC_INFO_LATESTRELEASE = "DOC_INFO_LATESTRELEASE";
	public static final String D_NAME = "dName";
	public static final String D_CAPTION = "dCaption";
	public static final String DOC_META_DEFINITION = "DocMetaDefinition";
	public static final String D_DOC_TITLE = "dDocTitle";
	public static final String D_DOC_TYPE = "dDocType";
	public static final String D_DOC_ACCOUNT = "dDocAccount";
	public static final String D_SECURITY_GROUP = "dSecurityGroup";
	public static final String SECURE = "Secure";
	public static final String DOCUMENT = "Document";
	public static final String FORCE_DOWNLOAD_STREAM_TO_FILE_PATH = "ForceDownloadStreamToFilepath";
	public static final String DATABINDER = "DataBinder";
	public static final String USERDATA = "UserData";
	public static final String PAGEMERGER = "PageMerger";
	public static final String D_EMAIL = "dEmail";

	// Search
	public static final String QUERY_TEXT = "QueryText";
	public static final String SEARCH_RESULTS = "SearchResults";
	public static final String RESULT_COUNT = "ResultCount";
	public static final int RESULT_COUNT_20 = 20;
	public static final String START_ROW = "StartRow";
	public static final String TOTAL_ROWS = "TotalRows";
	public static final String SORT_FIELD = "SortField";
	public static final String D_IN_DATE = "dInDate";
	public static final String SORT_ORDER = "SortOrder";
	public static final String DESC = "Desc";
	public static final String GET_SEARCH_RESULTS = "GET_SEARCH_RESULTS";
	public static final String SEARCH = "Search";

	public static final String FIELD_CAPTION = "fieldCaption";
	public static final String LOCAL_DATA = "LocalData";
	public static final String REMOTE_USER = "REMOTE_USER";
	public static final String DP_NAME = "dpName";
	public static final String DP_ACTION = "dpAction";
	public static final String PAGE_MERGER = "PageMerger";

	// Misc
	public static final String COMMA = ",";
	public static final String DOT_CSV = ".csv";
	public static final String COLON = ":";
	public static final String EQUALS = "=";
	public static final String AMPERSAND = "&";
	public static final String UTF8 = "UTF-8";
}
