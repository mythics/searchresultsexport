package com.mythics.webcenter.content;

import static com.mythics.webcenter.content.WebCenterContentStringConstants.DOC_INFO;
import static com.mythics.webcenter.content.WebCenterContentStringConstants.DOC_INFO_LATESTRELEASE;
import static com.mythics.webcenter.content.WebCenterContentStringConstants.DP_ACTION;
import static com.mythics.webcenter.content.WebCenterContentStringConstants.D_DOC_NAME;
import static com.mythics.webcenter.content.WebCenterContentStringConstants.GET_SEARCH_RESULTS;
import static com.mythics.webcenter.content.WebCenterContentStringConstants.IDC_SERVICE;
import static com.mythics.webcenter.content.WebCenterContentStringConstants.QUERY_TEXT;
import static com.mythics.webcenter.content.WebCenterContentStringConstants.RESULT_COUNT;
import static com.mythics.webcenter.content.WebCenterContentStringConstants.SEARCH;
import static com.mythics.webcenter.content.WebCenterContentStringConstants.SEARCH_RESULTS;
import static com.mythics.webcenter.content.WebCenterContentStringConstants.SORT_FIELD;
import static com.mythics.webcenter.content.WebCenterContentStringConstants.SORT_ORDER;
import static com.mythics.webcenter.content.WebCenterContentStringConstants.START_ROW;
import static com.mythics.webcenter.content.WebCenterContentStringConstants.TOTAL_ROWS;
import static intradoc.data.DataBinderUtils.getLocalInteger;
import static java.util.concurrent.TimeUnit.HOURS;
import static java.util.concurrent.TimeUnit.MINUTES;
import static java.util.concurrent.TimeUnit.NANOSECONDS;
import static java.util.concurrent.TimeUnit.SECONDS;
import intradoc.common.Report;
import intradoc.common.ServiceException;
import intradoc.data.DataBinder;
import intradoc.data.DataException;
import intradoc.data.DataResultSet;
import intradoc.server.Service;
import intradoc.server.ServiceData;
import intradoc.server.ServiceManager;

import java.util.Calendar;
import java.util.Properties;

/**
 * @author Jonathan Hult
 */
public class WebCenterContentHelperUtils {

	// default tracing section for these methods
	private static final String TRACE_SECTION = "webcentercontenthelperutils";

	/**
	 * This method executes a service.
	 * 
	 * @param serviceBinder
	 *            DataBinder containing service parameters.
	 * @param m_service
	 *            ServiceHandler object.
	 * 
	 * @return DataBinder containing service response.
	 * @throws ServiceException
	 */
	public static DataBinder executeService(final DataBinder serviceBinder, final Service m_service) throws ServiceException {
		traceVerbose("Start executeService");

		// Get service name
		final String serviceName = serviceBinder.getLocal(IDC_SERVICE);
		trace("Calling service " + serviceName + ": " + serviceBinder.getLocalData().toString());

		try {
			// This does not execute updateTopicInformation which is needed to call SAVE_USER_TOPICS to empty the content basket
			// m_service.getRequestImplementor().executeServiceTopLevelSimple(serviceBinder, serviceName, m_service.getUserData());

			// Execute service
			final ServiceData serviceData = ServiceManager.getFullService(serviceName);
			final Service service = ServiceManager.createService(serviceData.m_classID, m_service.getWorkspace(), null, serviceBinder, serviceData);
			service.initDelegatedObjects();
			m_service.getRequestImplementor().doRequestInternalEx(service, m_service.getUserData(), false);

			// Required for SAVE_USER_TOPICS to empty the content basket
			service.updateTopicInformation(serviceBinder);
			service.clear();

			trace("Finished calling service");
			return serviceBinder;
		} catch (final DataException e) {
			throw new ServiceException("Something went wrong executing service " + serviceName, e);
		} finally {
			traceVerbose("End executeService");
		}
	}

	/**
	 * This method retrieves DOC_INFO using DOC_INFO_LATESTRELEASE for a specified dDocName.
	 * 
	 * @param dDocName
	 *            to retrieve DOC_INFO for.
	 * @param m_service
	 *            ServiceHandler object.
	 * @return DataResultSet containing DOC_INFO (should contain one row).
	 * @throws ServiceException
	 * @throws DataException
	 */
	public static DataResultSet getDocInfoLatestRelease(final String dDocName, final Service m_service) throws ServiceException {
		traceVerbose("Start getDocInfoLatestRelease");

		try {
			// Put dDocName in serviceBinder for DOC_INFO_LATESTRELEASE
			final DataBinder serviceBinder = new DataBinder();
			serviceBinder.putLocal(D_DOC_NAME, dDocName);
			serviceBinder.putLocal(IDC_SERVICE, DOC_INFO_LATESTRELEASE);

			// Get DOC_INFO_LATESTRELEASE for latest revision for metadata to
			// use
			// for CHECKIN_SEL
			executeService(serviceBinder, m_service);

			final DataResultSet docInfo = new DataResultSet();
			docInfo.copy(serviceBinder.getResultSet(DOC_INFO));
			docInfo.first();
			return docInfo;
		} finally {
			traceVerbose("End getDocInfoLatestRelease");
		}
	}

	/**
	 * This method gets all possible search results from the index (includes only latest revisions; not limited to 200 results). This method executes GET_SEARCH_RESULTS in a loop until all results have been retrieved.
	 * 
	 * @param query
	 *            The query to execute.
	 * @param sortField
	 *            The field to sort on. Default is dInDate.
	 * @param sortOrder
	 *            asc or desc. Default is desc.
	 * @param service
	 *            Service.
	 * @return DataBinder containing LocalData and DataResultSet containing all search results.
	 * @throws ServiceException
	 */
	public static DataBinder getAllSearchResults(final String query, final String sortField, final String sortOrder, final Service service) throws ServiceException {
		traceVerbose("Enter getAllSearchResults");

		Properties localData = new Properties();
		final DataResultSet searchResults = new DataResultSet();
		final DataBinder data = new DataBinder();
		try {
			DataBinder serviceBinder = getSearchResults(query, 1, 200, sortField, sortOrder, service);
			localData = serviceBinder.getLocalData();
			searchResults.copy(serviceBinder.getResultSet(SEARCH_RESULTS));

			final int totalRows = getLocalInteger(serviceBinder, TOTAL_ROWS, 0);
			trace("TotalRows: " + totalRows);

			final int numTimesToSearch = totalRows / 200;
			trace("numTimesToSearch: " + numTimesToSearch);

			int startRow = 1;
			for (int i = 0; i < numTimesToSearch; i++) {
				startRow += 200;
				serviceBinder = getSearchResults(query, startRow, 200, sortField, sortOrder, service);
				final DataResultSet currentResults = new DataResultSet();
				currentResults.copy(serviceBinder.getResultSet(SEARCH_RESULTS));
				for (currentResults.first(); currentResults.isRowPresent(); currentResults.next()) {
					searchResults.addRowWithList(currentResults.getCurrentRowAsList());
				}
			}
			trace("Total number of all search results retrieved: " + searchResults.getNumRows());
			data.addResultSet(SEARCH_RESULTS, searchResults);
			data.setLocalData(localData);
			return data;
		} finally {
			traceVerbose("Exit getAllSearchResults");
		}
	}

	/**
	 * Get search results from the index (includes only latest revisions).
	 * 
	 * @param query
	 *            The query to execute.
	 * @param sortField
	 *            The field to sort on. Default is dInDate.
	 * @param sortOrder
	 *            asc or desc. Default is desc.
	 * @param service
	 *            Service.
	 * @param startRow
	 *            The starting row for the search.
	 * @param resultCount
	 *            The number of results to retrieve.
	 * @return DataBinder containing LocalData and DataResultSet containing all search results.
	 * @throws ServiceException
	 */
	public static DataBinder getSearchResults(final String query, final int startRow, final int resultCount, final String sortField, final String sortOrder, final Service service) throws ServiceException {
		traceVerbose("Enter getSearchResults");

		try {
			DataBinder serviceBinder = new DataBinder();
			serviceBinder.putLocal(IDC_SERVICE, GET_SEARCH_RESULTS);
			serviceBinder.putLocal(QUERY_TEXT, query);
			serviceBinder.putLocal(RESULT_COUNT, String.valueOf(resultCount));
			serviceBinder.putLocal(START_ROW, String.valueOf(startRow));
			serviceBinder.putLocal(SORT_FIELD, sortField);
			serviceBinder.putLocal(SORT_ORDER, sortOrder);
			serviceBinder.putLocal(DP_ACTION, SEARCH);
			serviceBinder = executeService(serviceBinder, service);
			traceVerbose("TotalRows for this search: " + serviceBinder.getLocal(TOTAL_ROWS));
			return serviceBinder;
		} finally {
			traceVerbose("Exit getSearchResults");
		}
	}

	/**
	 * Clear all the LocalData in the specified DataBinder.
	 * 
	 * @param binder
	 *            DataBinder where LocalData is to be cleared.
	 */
	public static void clearBinderLocalData(final DataBinder binder) {
		traceVerbose("Enter clearBinderLocalData");

		try {
			traceVerbose("DataBinder before clearing: " + binder.getLocalData().toString());
			binder.setLocalData(new Properties());
		} finally {
			traceVerbose("End clearBinderLocalData");
		}
	}

	/**
	 * Log the elapsed time (in minutes and seconds) since the start time.
	 * 
	 * @param startTime
	 *            The start time in nanoseconds.
	 */
	public static void logElapsedTime(final long startTimeInNanos) {
		final long endTime = System.nanoTime();
		final long elapsedTime = endTime - startTimeInNanos;

		final long hr = NANOSECONDS.toHours(elapsedTime);
		final long min = NANOSECONDS.toMinutes(elapsedTime - HOURS.toNanos(hr));
		final long sec = NANOSECONDS.toSeconds(elapsedTime - HOURS.toNanos(hr) - MINUTES.toNanos(min));
		final long ms = NANOSECONDS.toMillis(elapsedTime - HOURS.toNanos(hr) - MINUTES.toNanos(min) - SECONDS.toNanos(sec));

		trace("End time: " + Calendar.getInstance().getTime().toString());
		trace("Execution took " + String.format("%02d hours, %02d minutes, %02d seconds, %03d milliseconds", hr, min, sec, ms));
	}

	public static void trace(final String message, final String section) {
		trace(message, section, null);
	}

	private static void trace(final String message) {
		trace(message, TRACE_SECTION, null);
	}

	public static void trace(final String message, final String section, final Exception e) {
		Report.trace(section, message, e);
	}

	public static void traceVerbose(final String message, final String section) {
		if (Report.m_verbose) {
			trace(message, section);
		}
	}

	private static void traceVerbose(final String message) {
		traceVerbose(message, TRACE_SECTION);
	}

	public static void warn(final String message, final String section, final Exception e) {
		trace(message, section, e);
		Report.warning(section, message, e);
	}
}
